(in-package :groupbuy)

(define-url-function normal-user (request "GroupBuy - Normal User" (message string "You are normal-user."))
  (:p (fmt (format nil "Welcome to GroupBuy, ~a."
		   (if (search-value-by-key (get-cookie-values request) "user-name")
		       (search-value-by-key (get-cookie-values request) "password")
		       (search-value-by-key (request-query request) "user-name")))))
  (:p (fmt (format nil "Message: ~a" message)))
  (:p "If you have a new tendency, you can fill in a new form "
      (:a :href "/new-tendency" "here") ".")
  (:p (:a :href "/show-tendency" "To show tendency")))

;;; to search for user-name in cookie you can
;; (search-value-by-key (get-cookie-values request))