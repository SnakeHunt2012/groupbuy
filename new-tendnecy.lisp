(in-package :groupbuy)

(define-url-function new-tendency (request "GroupBuy - New Tendency")
  (:p "Please fill in the form below:")
  (:form :method "POST" :action "/add-tendency"
	 (:table
	  (:tr (:td "Where do you want to go")
	       (:td (:input :name "location" :size 40)))
	  (:tr (:td "How far can you bear")
	       (:td (:input :name "distance" :size 40)))
	  (:tr (:td "How long do you want to stay")
	       (:td (:input :name "days" :size 40)))
	  (:tr (:td "When do you want to come")
	       (:td (:input :name "time" :size 40)))
	  (:tr (:td "Some other requirements" :value "Submit")
	       (:td (:input :name "requirements" :size 40))))
	 (:p (:input :name "submit" :type "submit" :value "Submit")
	     (:input ::type "reset" :value "Reset"))))