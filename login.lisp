(in-package :groupbuy)

(define-url-function login (request "GroupBuy - Login")
  (:p "Welcome to GroupBuy, please login:")
  (:form :method "POST" :action "/dispatcher"
	 (:table
	  (:tr (:td "User Name")
	       (:td (:input :name "user-name" :size 20)))
	  (:tr (:td "Password")
	       (:td (:input :name "password" :type "password" :size 20))))
	 (:p
	  (:input :name "submit" :type "submit" :value "Okey")
	  (:input ::type "reset" :value "Reset")))
  (:p "If you don't have an account now, you can "
      (:a :href "/sign-up" "sign up") "."))
