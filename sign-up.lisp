(in-package :groupbuy)

(define-url-function sign-up (request "GroupBuy - Sign Up")
  (:p "Please enter your information:")
  (:form :method "POST" :action "/register"
	 (:table
	  (:tr (:td "User Name")
	       (:td (:input :name "user-name" :size 20)))
	  (:tr (:td "Password")
	       (:td (:input :name "password" :type "password" :size 20)))
	  (:tr (:td "Email")
	       (:td (:input :name "email" :size 20)))
	  (:tr (:td "Phone")
	       (:td (:input :name "phone" :size 20)))
	  (:tr (:td "Address")
	       (:td (:input :name "address" :size 20))))
	 (:p (:input :name "submit" :type "submit" :value "Submit")
	     (:input ::type "reset" :value "Reset"))))