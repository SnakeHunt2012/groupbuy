(in-package :groupbuy)

(define-url-function register (request "GroupGuy - Login Succeed")
  (:p "Please rememter your profile:")
  (loop for item in (request-query request) do
       (htm (:p (fmt (format nil "~a : ~a" (car item) (cdr item))))))
  (add-user-register request)
  (if (search-value-by-key (request-query request) "type")
      (htm (:p (:a :href "/administer" "Back?")))
      (htm (:p (:a :href "/login" "Go to login?")))))