(in-package :groupbuy)

(define-url-function add-user (request "GroupBuy - Add User")
  (:p "To add a user:")
  (:form :method "POST" :action "/register"
	 (:table (:tr (:td "User type")
		      (:td (:input :name "type" :size 20)))
		 (:tr (:td "User Name")
		      (:td (:input :name "user-name" :size 20)))
		 (:tr (:td "Password")
		      (:td (:input :name "password" :type "password" :size 20)))
		 (:tr (:td "Email")
		      (:td (:input :name "email" :size 20)))
		 (:tr (:td "Phone")
		      (:td (:input :name "phone" :size 20)))
		 (:tr (:td "Address")
		      (:td (:input :name "address" :size 20))))
	 (:p (:input :name "submit" :type "submit" :value "Okey")
	     (:input ::type "reset" :value "Reset"))))