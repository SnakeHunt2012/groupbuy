;;; load package
(in-package :groupbuy)

;;; test-cookies
(defun test-cookies (request entity)
  (with-http-response (request entity :content-type "text/html")
    (with-http-body (request entity)
      (let ((html-output (request-reply-stream request)))
	(with-html-output (out html-output)
	  (htm
	   (:html
	     (:head
	      (:title "Test Cookies"))
	     (:body
	      (if (null (get-cookie-values request))
		  (htm (:p "No cookies."))
		  (htm
		   (:table
		    (loop for (key . value) in (get-cookie-values request)
			 do (htm (:tr (:td (fmt (format nil "~a" key))) (:td (fmt (format nil "~a" value)))))))))))))))))

;;; release
(publish :path "/test-cookies" :function 'test-cookies)