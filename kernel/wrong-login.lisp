;;; load package
(in-package :groupbuy)

;;; wrong-login
(defun wrong-login (request entity)
  (with-http-response (request entity :content-type "text/html")
    (with-http-body (request entity)
      (let ((html-output (request-reply-stream request)))
	(with-html-output (out html-output)
	  (htm
	  (:html
	    (:head
	     (:title "Wrong Output")
	     (:style :type "text/css" "
body {
    background-color: #efefef;
    padding: 12px;
}

#outer {
    background: none repeat scroll 0 0 #ffffff;
    margin: 0 auto;
    padding: 10px;
    text-align: left;
    width: 960px;
}

#container {
    border: 1px solid #eaeaea;
}

#header {
    background-color: #eaeaea;
    height: 70px;
    margin: 0;
    padding: 0;
}

#header h1 {
    height: 70px;
    margin: 0;
    color: #7a7c85;
    float: left;
    padding-left: 20px;
    font: 30px/70px Georigia, Times, serif;
}

#content {
    padding: 10px;
}

input {
    border-width: 1px;
    border-color: #CCCCCC;
    border-style: solid;
    margin: 5px;
}
                                      "))
	    (:body
	     (:div :id "outer"
		   (:div :id "container"
			 (:div :id "header"
			       (:h1 "GroupBuy - Wrong Login"))
			 (:div :id "content"
	     (:p "Username dose not exist.")
	     (:p (:a :href "/test-cookies" "Look at cookie jar.")))))))))))))

(publish :path "/wrong-login" :function 'wrong-login)
