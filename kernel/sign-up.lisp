;;; load package
(in-package :groupbuy)

;;; sign up
(defun sign-up (request entity)
  (with-http-response (request entity :content-type "text/html")
    (with-http-body (request entity)
      (let ((html-output (request-reply-stream request)))
	(with-html-output (out html-output)
	  (htm
	   (:html
	     (:head
	      (:title "Sign up")
	      (:style :type "text/css" "
body {
    background-color: #efefef;
    padding: 12px;
}

#outer {
    background: none repeat scroll 0 0 #ffffff;
    margin: 0 auto;
    padding: 10px;
    text-align: left;
    width: 960px;
}

#container {
    border: 1px solid #eaeaea;
}

#header {
    background-color: #eaeaea;
    height: 70px;
    margin: 0;
    padding: 0;
}

#header h1 {
    height: 70px;
    margin: 0;
    color: #7a7c85;
    float: left;
    padding-left: 20px;
    font: 30px/70px Georigia, Times, serif;
}

#content {
    padding: 10px;
}

input {
    border-width: 1px;
    border-color: #CCCCCC;
    border-style: solid;
    margin: 5px;
}
                                       "))
	     (:body
	      (:div :id "outer"
		    (:div :id "container"
			  (:div :id "header"
				(:h1 "GroupBuy - Sign Up"))
			  (:div :id "content"
	      (:p "Please enter some information:")
	      (:form :method "POST" :action "/register"
		     (:table
		      (:tr (:td "User Name")
			   (:td (:input :name "user-name" :size 20)))
		      (:tr (:td "Password")
			   (:td (:input :name "password" :type "password" :size 20))))
		     (:P (:input :name "submit" :type "submit" :value "Submit")
			 (:input ::type "reset" :value "Reset"))))))))))))))

;;; release
(publish :path "/sign-up" :function 'sign-up)
