;;; load package
(in-package :groupbuy)

;;; send-offer
(defun send-offer (request entity)
  (progn
    (offer-send (cdar (request-query request)))
    (with-http-response (request entity :content-type "text/html")
      (administer request entity))))

;;; release
(publish :path "/send-offer" :function 'send-offer)