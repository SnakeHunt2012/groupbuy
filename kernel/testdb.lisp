;;; load package
(in-package :groupbuy)

;;; test
(defvar *test-db-spec*)
(setf *test-db-spec*
      '(:CLSQL (:POSTGRESQL "127.0.0.1" "mydb" "posgresql" "1218503952")))
(groupbuy:open-store *test-db-spec*)