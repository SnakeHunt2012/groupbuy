;;; load package
(in-package :groupbuy)

;;; delete-tendency
(defun delete-tendency (request entity)
  (progn
    (remove-tendency (cdar (nthcdr 0 (request-query request))))
    (with-http-response (request entity :content-type "text/html")
      (administer request entity))))

;;; release
(publish :path "/delete-tendency" :function 'delete-tendency)