;;; load package
(in-package :groupbuy)

;;; register
(defun register (request entity)
  (with-http-response (request entity :content-type "text/html")
    (with-http-body (request entity)
      (let ((html-output (request-reply-stream request)))
	(with-html-output (out html-output)
	  (htm
	   (:html
	     (:head
	      (:title "Register"))
	     (:body
	      (if (string/= (cdar (request-query request)) "")
		  (if (string/= (cdadr (request-query request)) "")
		      (progn 
			(push
			 (make-instance
			  'user
			  :user-name (cdar (request-query request))
			  :password (cdadr (request-query request))
			  :user-type (if (string= (caaddr (request-query request)) "user-type")
				      (cdaddr (request-query request))
				      2))
			 *user-list*)
			(htm
			 (:p "Success:")
			 (:p "User Name - " (fmt (format nil "~a" (cdar (request-query request)))))
			 (:p "Password - " (fmt (format nil "~a" (cdadr (request-query request)))))
			 (:p "User-type - " (if (string= (caaddr (request-query request)) "user-type")
				 (fmt (format nil "~A" (cdaddr (request-query request))))
				 (fmt (format nil "normal-user"))))))
		      (htm
		       (:p "Plese enter your Password.")))
		  (htm
		   (:p "Please enter your User name." (fmt (format nil (cdar (request-query request)))) )))))))))))

;;; release
(publish :path "/register" :function 'register)