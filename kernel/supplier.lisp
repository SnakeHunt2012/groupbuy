;;; load package
(in-package :groupbuy)

;;; supplier
(defun supplier (request entity)
  (with-http-response (request entity :content-type "text/html")
    (with-http-body (request entity)
      (let ((html-output (request-reply-stream request)))
	(with-html-output (out html-output)
	  (htm
	   (:html
	     (:head
	      (:title "Supplier")
	      (:style :type "text/css"
		      "
body {
    background-color: #efefef;
    padding: 12px;
}

#outer {
    background: none repeat scroll 0 0 #ffffff;
    margin: 0 auto;
    padding: 10px;
    text-align: left;
    width: 960px;
}

#container {
    border: 1px solid #eaeaea;
}

#header {
    background-color: #eaeaea;
    height: 70px;
    margin: 0;
    padding: 0;
}

#header h1 {
    height: 70px;
    margin: 0;
    color: #7a7c85;
    float: left;
    padding-left: 20px;
    font: 30px/70px Georigia, Times, serif;
}

#content {
    padding: 10px;
}

input {
    border-width: 1px;
    border-color: #CCCCCC;
    border-style: solid;
    margin: 5px;
}

.item p {
    background-color: #efefef;
    padding: 10px;
    border-width: 1px;
    border-style: solid;
    border-color: #CCCCCC;
}

.item p:hover {
    background-color: #cccccc;
}
                      "))
	     (:body
	      (:div :id "outer"
		    (:div :id "container"
			  (:div :id "header"
				(:h1 "GroupBuy - Supplier"))
			  (:div :id "content"
				(:p "You are supplier")
					;(:p "If you want to add a good, please fill the form behind:")
					;(:form :method "POST" :action "/add-good"
					;	     (:table
					;	      (:tr (:td "Good Name")
					;		   (:td (:input :name "name" :size)))
					;	      (:tr (:td "Good Price")
					;		   (:td (:input :name "price" :size)))
					;	      (:tr (:td "Amount to distribute")
					;		   (:td (:input :name "amount" :size))))
					;	     (:p (:input :name "submit" :type "submit" :value "Okay")
					;		 (:input ::type "reset" :value "Reset")))
				(:p "Current tendency:")
				(loop for i in *tendency-list*
				   do (htm (:div :class "item" (:p (fmt (format nil
							    "ID: ~A<br/>
                                                             Location: ~A<br/>
                                                             Distance: ~A<br/>
                                                             Days: ~A<br>
                                                             price:~A<br/>
                                                             Require:~A</br>
                                                             Votes:~A</br>"
							    (slot-value i 'tendency-id)
							    (slot-value i 'location)
							    (slot-value i 'distance)
							    (slot-value i 'days)
							    (slot-value i 'price)
							    (slot-value i 'requirements)
							    (slot-value i 'vote)))))))
				(:p (:a :href "/test-cookies" "Look add cookie jar.")))))))))))))

;;; release
(publish :path "/supplier" :function 'supplier)
