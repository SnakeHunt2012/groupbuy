;;; load package
(in-package :groupbuy)

;;; dispatcher
(defun dispatcher (request entity)
  (if (login-check *user-list* (cdar (request-query request)) (cdadr (request-query request)))
      (with-http-response (request entity :content-type "text/html")
	(set-cookie-header request :name "user-type" :value "normal-user")
	(normal-user request entity))
      (if (string= "supplier" (cdr (car (request-query request))))
	  (with-http-response (request entity :content-type "text/html")
	    (set-cookie-header request :name "user-type" :value "supplier")
	    (supplier request entity))
	  (if (string= "administer" (cdr (car (request-query request))))
	      (with-http-response (request entity :content-type "text/html")
		(set-cookie-header request :name "user-type" :value "supplier")
		(administer request entity))
	      (with-http-response (request entity :content-type "text/html")
		(wrong-login request entity))))))

;;; release
(publish :path "/dispatcher" :function 'dispatcher)