;;; load package
(in-package :groupbuy)

;;; interface: administer
(defun administer (request entity)
  (with-http-response (request entity :content-type "text/html")
    (with-http-body (request entity)
      (let ((html-output (request-reply-stream request)))
	(with-html-output (out html-output)
	  (htm
	   (:html
	     (:head
	      (:title "Administer")
	      (:style :type "text/css" "

body {
    background-color: #efefef;
    padding: 12px;
}

#outer {
    background: none repeat scroll 0 0 #ffffff;
    margin: 0 auto;
    padding: 10px;
    text-align: left;
    width: 960px;
}

#container {
    border: 1px solid #eaeaea;
}

#header {
    background-color: #eaeaea;
    height: 70px;
    margin: 0;
    padding: 0;
}

#header h1 {
    height: 70px;
    margin: 0;
    color: #7a7c85;
    float: left;
    padding-left: 20px;
    font: 30px/70px Georigia, Times, serif;
}

#content {
    padding: 10px;
}

input {
    border-width: 1px;
    border-color: #CCCCCC;
    border-style: solid;
    margin: 5px;
}

.item p {
    background-color: #efefef;
    padding: 10px;
    border-color: #CCCCCC;
    border-style: solid;
    border-width: 1px;
}

.item p:hover {
    background-color: #cccccc;
}

.line {
    color: #ffffff;
}
                                       "))
	     (:body
	      (:div :id "outer"
		    (:div :id "container"
			  (:div :id "header"
				(:h1 "GroupBuy - Administer"))
			  (:div :id "content"
				(:p "To add user:")
				(:form :method "POST" :action "/register"
				       (:table
					(:tr (:td "User Name")
					     (:td (:input :name "user-name" :size 20)))
					(:tr (:td "Password")
					     (:td (:input :name "password" :size 20)))
					(:tr (:td "User Type")
					     (:td (:input :name "user-type" :size 20))))
				       (:p
					(:input :name "submit" :type "submit" :value "Okey")
					(:input ::type "reset" :value "Reset")))
				(:hr :class "line")
				(:p "To delete user")
				(:form :method "POST" :action "/delete-normal-user"
				       (:table
					(:tr (:td "User Name")
					     (:td (:input :name "user-name" :size 20))))
				       (:p
					(:input :name "submit" :type "submit" :value "Okey")
					(:input ::type "reset" :value "Reset")))
				(:hr :class "line")
				(:p "To delete tendency")
				(:form :method "POST" :action "/delete-tendency"
				       (:table
					(:tr (:td "Id")
					     (:td (:input :name "tendency-id" :size 20))))
				       (:p
					(:input :name "submit" :type "submit" :value "Okey")
					(:input ::type "reset" :value "Reset")))
				(:hr :class "line")
				(:p "To send offer")
				(:form :method "POST" :action "/send-offer"
				       (:table
					(:tr (:td "Id")
					     (:td (:input :name "tendency-id" :size 20))))
				       (:p
					(:input :name "submit" :type "submit" :value "Okey")
					(:input ::type "reset" :value "Reset")))
				(:hr :class "line")
				(:p "Current Tendency:")
				(loop for tendency in *tendency-list*
				   do (htm (:div :class "item" (:p (fmt (format nil
										"ID: ~A<br/>
                                                                                 location: ~A<br/>
                                                                                 distance: ~A<br/>
                                                                                 days: ~A<br/>
                                                                                 price: ~A<br/>
                                                                                 requirements: ~A<br/>
                                                                                 vote: ~A<br/>"
										(slot-value tendency 'tendency-id)
										(slot-value tendency 'location)
										(slot-value tendency 'distance)
										(slot-value tendency 'days)
										(slot-value tendency 'price)
										(slot-value tendency 'requirements)
										(slot-value tendency 'vote)))))))
				(:p "Current Users:")
				(loop for user in *user-list*
				   do (htm (:div :class "item" (:p (fmt (format nil
										"user-name: ~A <br/>
                                                                                 user-password: ~A <br/>
                                                                                 user-type: ~A <br/>"
										(slot-value user 'user-name)
										(slot-value user 'password)
										(slot-value user 'user-type)))))
					;(:p "Current Goods:")
					;(loop for i in *good-list*
					;   do (htm (:p (fmt (format nil
					;			    "Name:~A    Price:~A    Total:~A    Vote:~A"
					;			    (slot-value i 'name)
					;			    (slot-value i 'price)
					;			    (slot-value i 'amount)
					;			    (slot-value i 'vote))))))
					)))))))))))))

;;;b release
(publish :path "/administer" :function 'administer)
