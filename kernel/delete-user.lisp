;;; load package
(in-package :groupbuy)

;;; delete-normal-user
(defun delete-normal-user (request entity)
  (with-http-response (request entity :content-type "text/html")
    (with-http-body (request entity)
      (let ((html-output (request-reply-stream request)))
	(with-html-output (out html-output)
	  (if (delete-user (cdar (request-query request)))
	    (htm
	     (:html
	       (:head "Delete User")
	       (:body
		(:p "Delete Success."))))
	    (htm
	     (:html
	       (:head "Delete User")
	       (:body
		(:p "Delete Failed.")
		(:p (fmt (format nil "It is ~A" (cdar (request-query request))))))))))))))

;;; release
(publish :path "/delete-normal-user" :function 'delete-normal-user)