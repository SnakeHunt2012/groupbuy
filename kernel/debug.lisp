;;; load package
(in-package :groupbuy)

;;; debug
(defun debug-page (request entity)
  (with-http-response (request entity :content-type "text/html")
    (with-http-body (request entity)
      (let ((html-output (request-reply-stream request)))
	(with-html-output (out html-output)
	  (htm
	   (:html
	     (:head
	      (:title "Debug"))
	     (:body
	      (:p (fmt (format nil "~A" (parse-integer (cdar (nthcdr 0 (request-query request)))))))))))))))

;;; release
(publish :path "/debug-page" :function 'debug-page)