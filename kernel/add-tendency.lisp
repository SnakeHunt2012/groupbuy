;;; load package
(in-package :groupbuy)

;;; add-good
(defun add-good (request entity)
  (with-http-response (request entity :content-type "text/html")
    (with-http-body (request entity)
      (let ((html-output (request-reply-stream request)))
	(with-html-output (out html-output)
	  (progn
	    (push (make-instance
		   'tendency
		   :location (cdar (nthcdr 0 (request-query request)))
		   :distance (cdar (nthcdr 1 (request-query request)))
		   :days (cdar (nthcdr 2 (request-query request)))
		   :price (cdar (nthcdr 3 (request-query request)))
		   :requirements (cdar (nthcdr 4 (request-query request)))) *tendency-list*)
	    (htm
	     (:html
	       (:head
		(:title "Submit"))
	       (:body
		(:p "Add success!")
		(:p (fmt (format nil "Where do you want to go: ~A<br/>How far can you bear: ~A<br/>How long do you want to stay: ~A<br/>How much could you supply: ~A<br/>Some other requirements: ~A<br/>"
				 (cdar (nthcdr 0 (request-query request)))
				 (cdar (nthcdr 1 (request-query request)))
				 (cdar (nthcdr 2 (request-query request)))
				 (cdar (nthcdr 3 (request-query request)))
				 (cdar (nthcdr 4 (request-query request))))))
		(:p (:a :href "/normal-user" "Back?")))))))))))

;;; release
(publish :path "/add-good" :function 'add-good)