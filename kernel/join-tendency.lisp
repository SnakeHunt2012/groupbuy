;;; load package
(in-package :groupbuy)

;;; normal-user
(defun join-tendency (request entity)
  (progn
    (set-vote-by-id
     *tendency-list*
     (parse-integer (cdar (request-query request)))
     (+
      (search-vote-by-id *tendency-list* (parse-integer (cdar (request-query request))))
      (parse-integer (cdar (nthcdr 1 (request-query request))))))
    (normal-user request entity)))

;;; release
(publish :path "/join-tendency" :function 'join-tendency)