;;; load package
(in-package :groupbuy)

;;; normal-user
(defun normal-user (request entity)
  (with-http-response (request entity :content-type "text/html")
    (with-http-body (request entity)
      (let ((html-output (request-reply-stream request)))
	(with-html-output (out html-output)
	  (htm
	   (:html
	     (:head
	      (:title "Normal User")
	      (:style :type "text/css" 
		      "
body {
    background-color: #efefef;
    padding: 12px;
}

#outer {
    background: none repeat scroll 0 0 #ffffff;
    margin: 0 auto;
    padding: 10px;
    text-align: left;
    width: 960px;
}

#container {
    border: 1px solid #eaeaea;
}

#header {
    background-color: #eaeaea;
    height: 70px;
    margin: 0;
    padding: 0;
}

#header h1 {
    height: 70px;
    margin: 0;
    color: #7a7c85;
    float: left;
    padding-left: 20px;
    font: 30px/70px Georigia, Times, serif;
}

#content {
    padding: 10px;
}

input {
    border-width: 1px;
    border-color: #CCCCCC;
    border-style: solid;
    margin: 5px;
}

.item p {
    background-color: #efefef;
    padding: 10px;
    border-color: #CCCCCC;
    border-style: solid;
    border-width: 1px;
}

.item p:hover {
    background-color: #CCCCCC;
}
                      "
		      ))
	     (:body
	      (:div :id "outer"
		    (:div :id "container"
			  (:div :id "header"
				(:h1 "GroupBuy - Normal User"))
			  (:div :id "content"
				(:p "If there is no tendencys suit you, you can create a new tendency:")
				(:form :method "POST" :action "/add-good"
				       (:table
					(:tr (:td "Where do you want to go")
					     (:td (:input :name "location" :size 40)))
					(:tr (:td "How far can you bear")
					     (:td (:input :name "distance" :size 40)))
					(:tr (:td "How long do you want to stay:")
					     (:td (:input :name "days" :size 40)))
					(:tr (:td "How much could you supply")
					     (:td (:input :name "price" :size 40)))
					(:tr (:td "Some other requirements")
					     (:td (:input :name "requirements" :size 40))))
				       (:p (:input :name "submit" :type "submit" :value "Submit")
					   (:input ::type "reset" :value "Reset")))
				(:p "Other's tendency:")
				(loop for i in *tendency-list*
				   do (htm (:div :class "item" (:p 
						  (fmt (format nil "ID:~A<br/>~@
                                                                    Location:~A<br/>~@
                                                                    Distance:~A<br/>~@
                                                                    Days:~A<br/>~@
                                                                    Price:~A<br/>~@
                                                                    Requirements:~A<br/>~@
                                                                    Vote:~A<br/>"
							       (slot-value i 'tendency-id)
							       (slot-value i 'location)
							       (slot-value i 'distance)
							       (slot-value i 'days)
							       (slot-value i 'price)
							       (slot-value i 'requirements)
							       (slot-value i 'vote))))
					    (:p "If you want to add to this tendency, please fill in the form:")
					    (:form :method "POST" :action (format nil "/join-tendency?tendency-id=~A " (slot-value i 'tendency-id))
						   (:table
						    (:tr
						     (:td "How much people")
						     (:td (:input :name "votes" :size 5)
							  (:input :name "submit" :type "submit" :value "Submit")
							  (:input ::type "reset" :value "Reset")))))))))
	     (:p (:a :href "/test-cookies" "Look at cookie jar."))))))))))))

;;; release
(publish :path "/normal-user" :function 'normal-user)