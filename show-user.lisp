(in-package :groupbuy)

(define-url-function show-user (request "GroupBuy - Show User")
  (:p "All users in current list")
  (loop for user in *user-list* do
       (htm
	(:div :class "item"
	      (:a :href (format nil "/show-user-profile?user-name=~a"
				(slot-value user 'user-name))
		  (:p
		   (fmt
		    (format nil
			    "user-id: ~a<br/>
                             user-name: ~a<br/>
                             user-password: ~a<br/>
                             user-type: ~a<br/>
                             user-email: ~a<br/>
                             user-phone: ~a<br/>
                             user-address: ~a<br/>"
			    (slot-value user 'id)
			    (slot-value user 'user-name)
			    (slot-value user 'password)
			    (slot-value user 'type)
			    (slot-value (slot-value user 'profile) 'email)
			    (slot-value (slot-value user 'profile) 'phone)
			    (slot-value (slot-value user 'profile) 'address)))))))))