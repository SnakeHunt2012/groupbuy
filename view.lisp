(in-package :groupbuy)

(defun normalize-param (param)
  (etypecase param
    (list param)
    (symbol `(,param string nil nil))))

(defun get-cookie-value (request name)
  (cdr (assoc name (get-cookie-values request) :test #'string=)))

(defun symbol->query-name (sym)
  (string-downcase sym))

(defun symbol->cookie-name (function-name sym sticky)
  (let ((package-name (package-name (symbol-package function-name))))
    (when sticky
      (ecase sticky
	(:global
	 (string-downcase sym))
	(:package
	 (format nil "~(~a:~a~)" package-name sym))
	(:local
	 (format nil "~(~a:~a:~a~)" package-name function-name sym))))))

(defun set-cookie-code (function-name request param)
  (destructuring-bind (name type &optional default sticky) param
    (declare (ignore type default))
    (if sticky
	`(when ,name
	   (set-cookie-header
	    ,request
	    :name ,(symbol->cookie-name function-name name sticky)
	    :value (princ-to-string ,name))))))

(defun set-cookies-code (function-name request params)
  (loop for param in params
       when (set-cookie-code function-name request param) collect it))

(defgeneric string->type (type value))

(defmethod string->type ((type (eql 'string)) value)
  (and (plusp (length value)) value))

(defmethod string->type ((type (eql 'integer)) value)
  (parse-integer (or value "") :junk-allowed t))

(defun param-binding (function-name request param)
  (destructuring-bind (name type &optional default sticky) param
    (let ((query-name (symbol->query-name name))
	  (cookie-name (symbol->cookie-name function-name name sticky)))
      `(,name (or
	       (string->type ',type (request-query-value ,query-name ,request))
	       ,@(if cookie-name
		     (list `(string->type ',type
			     (get-cookie-value ,request ,cookie-name))))
	       ,default)))))

(defun param-bindings (function-name request params)
  (loop for param in params
       collect (param-binding function-name request param)))

(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym)))
     ,@body))

(defmacro define-url-function (name (request title &rest params) &body body)
  (with-gensyms (entity)
    (let ((params (mapcar #'normalize-param params)))
      `(progn
	 (defun ,name (,request ,entity)
	   (with-http-response (,request ,entity :content-type "text/html")
	     (let* (,@(param-bindings name request params))
	       ,@(set-cookies-code name request params)
	       (with-http-body (,request ,entity)
		 (let ((html-output (request-reply-stream request)))
		  (with-html-output (out html-output)
		    (htm
		     (:html
		       (:head (:title ,title)
			      (:style :type "text/css"
				      "
body {
    background-color: #efefef;
    padding: 12px;
}

#outer {
    background: none repeat scroll 0 0 #ffffff;
    margin: 0 auto;
    padding: 10px;
    text-align: left;
    width: 960px;
}

#container {
    border: 1px solid #eaeaea;
}

#header {
    background-color: #eaeaea;
    height: 70px;
    margin: 0;
    padding: 0;
}

#header h1 {
    height: 70px;
    margin: 0;
    color: #7a7c85;
    float: left;
    padding-left: 20px;
    font: 30px/70px Georigia, Times, serif;
}

#content {
    padding: 10px;
    padding-left: 20px;
}

fieldset {
    border-width: 1px;
    border-color: #CCCCCC;
    border-style: solid;
    margin-right: 25px;
    margin-left: 15px;
    margin-top: 20px;
    margin-bottom: 20px;
}

legend {
    font-size: 20px;
    font-style: italic;
    color: #7a7c85;
}

input {
    border-width: 1px;
    border-color: #CCCCCC;
    border-style: solid;
    margin: 5px;
}

select {
    border-width: 1px;
    border-color: #CCCCCC;
    border-style: solid;
    margin: 5px;
}

textarea {
    border-width: 1px;
    border-color: #CCCCCC;
    border-style: solid;
    margin: 5px;
}

.item p {
    background-color: #efefef;
    padding: 10px;
    border-color: #CCCCCC;
    border-style: solid;
    border-width: 1px;
}

.item p:hover {
    background-color: #CCCCCC;
}

.item a {
    text-decoration: none;
    color: #000000;
}
                                      "))
		       (:body
			(:div :id "outer"
			      (:div :id "container"
				    (:div :id "header"
					  (:h1 ,title))
				    (:div :id "content"
				    ,@body))))))))))))
	 (publish :path ,(format nil "/~(~a~)" name) :function ',name)))))
