(in-package :groupbuy)

(define-url-function show-tendency-user (request "GroupBuy - Users in Tendency")
  (loop for tendency in *tendency-list* do
       (when (= (parse-integer
		 (search-value-by-key
		  (request-query request)
		  "tendency-id"))
		(slot-value tendency 'id))
	 (htm (:p (fmt (format nil "tendency-id: ~a"
			       (slot-value tendency 'id))))
	      (:p (fmt (format nil "destination: ~a"
			       (slot-value tendency 'location))))
	      (:p (fmt (format nil "max distance: ~a"
			       (slot-value tendency 'distance))))
	      (:p (fmt (format nil "days in the hotel: ~a"
			       (slot-value tendency 'days))))
	      (:p (fmt (format nil "time to arrive: ~a"
			       (slot-value tendency 'time))))
	      (:p (fmt (format nil "requirements: ~a"
			       (slot-value tendency 'requirements))))
	      (:p (fmt (format nil "vote: ~a"
			       (count-number
				(slot-value tendency 'user-list) 0))))
	      (:hr :class "line")
	      (:form :method "POST" :action (format nil "/join-tendency?tendency-id=~a"
						    (search-value-by-key
						     (request-query request)
						     "tendency-id"))
		     (:table
		      (:p (:input :name "vote"
				  :type "submit"
				  :value "Vote to This?"))))
	      (:p "Users who have this tendency are listed below:")
	      (loop for user in (slot-value tendency 'user-list) do
		   (htm
		    (:div :class "item"
			  (:a :href (format nil "/show-user-profile?user-name=~a"
					    user)
			      (:p (fmt (format nil "~a" user)))))))))))