(in-package :groupbuy)

(define-url-function wrong-login (request "GroupBuy - Wrong Login" (message string "Something wrong with your user name or password."))
  (:p (fmt (format nil "Message: ~a" message)))
  (:p (fmt (format nil "User Name: ~a" (search-value-by-key (request-query request) "user-name"))))
  (:p (fmt (format nil "Password: ~a" (search-value-by-key (request-query request) "password")))))