(in-package :groupbuy)

(define-url-function show-user-profile (request "GroupBuy - User Profile")
  (loop for user in *user-list* do
       (when (string=
	      (search-value-by-key (request-query request) "user-name")
	      (slot-value user 'user-name))
	 (htm
	  (:p (fmt (format nil "user-id: ~a"
			   (slot-value user 'id))))
	  (:p (fmt (format nil "user-name ~a"
			   (slot-value user 'user-name))))
	  (:p (fmt (format nil "user-type: ~a"
			   (slot-value user 'type))))
	  (:p (fmt (format nil "user-email: ~a"
			   (slot-value
			    (slot-value user 'profile)
			    'email))))
	  (:p (fmt (format nil "user-phone: ~a"
			   (slot-value
			    (slot-value user 'profile)
			    'phone))))
	  (:p (fmt (format nil "user-address: ~a"
			   (slot-value
			    (slot-value user 'profile)
			    'address))))))))