(in-package :groupbuy)

(defun dispatcher (request entity)
  (cond
    ((login-check request *user-list* 2)
     (with-http-response (request entity :content-type "text/html")
       (set-cookie-header request
			  :name "type"
			  :value "2")
       (set-cookie-header request
			  :name "user-name"
			  :value (format nil "~a"
					 (search-value-by-key
					  (request-query request) "user-name")))
       (normal-user request entity)))
    ((login-check request *user-list* 1)
     (with-http-response (request entity :content-type "text/html")
       (set-cookie-header request
			  :name "type"
			  :value "supplier")
       (set-cookie-header request
			  :name "user-name"
			  :value (format nil "~a"
					 (search-value-by-key
					  (request-query request) "user-name")))
       (supplier request entity)))
    ((login-check request *user-list* 0)
     (with-http-response (request entity :content-type "text/html")
       (set-cookie-header request
			  :name "type"
			  :value "administer")
       (set-cookie-header request
			  :name "user-name"
			  :value (format nil "~a"
					 (search-value-by-key
					  (request-query request) "user-name")))
       (administer request entity)))
    (t
     (with-http-response (request entity :content-type "text/html")
       (wrong-login request entity)))))

(publish :path "/dispatcher" :function 'dispatcher)
