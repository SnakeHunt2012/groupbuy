;;; -*- Mode: Lisp -*-

(defpackage :groupbuy-system
  (:use :cl :asdf))

(in-package :groupbuy-system)

(defsystem :groupbuy
  :name "groupbuy"
  :author "SnakeHunt2012 <SnakeHunt2012@gmail.com>"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
	       (:file "config")
	       (:file "groupbuy")

	       ;;; kernel
	       (:file "view")
	       (:file "common")
	       (:file "register")
	       
	       ;;; controller
	       (:file "add-user")
	       (:file "dispatcher")  ; actually it need administer and supplier and normal-user and wrong-login
	       (:file "add-tendency")
	       (:file "show-user")
	       (:file "show-tendency")
	       (:file "show-tendency-user")
	       (:file "show-user-profile")
	       (:file "join-tendency")
	       (:file "sign-up")
	       (:file "login")
	       (:file "new-tendency")

	       ;;; view
	       (:file "administer")
	       (:file "supplier")
	       (:file "normal-user")
	       (:file "wrong-login")))
