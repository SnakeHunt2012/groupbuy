(in-package :groupbuy)

(defun add-tendency (request entity)
  (push-tendency
   (search-value-by-key (get-cookie-values request) "user-name")
   (search-value-by-key (request-query request) "location")
   (search-value-by-key (request-query request) "distance")
   (search-value-by-key (request-query request) "days")
   (search-value-by-key (request-query request) "time")
   (search-value-by-key (request-query request) "room-amount")
   (search-value-by-key (request-query request) "room-type")
   (search-value-by-key (request-query request) "requirements"))
  (with-http-response (request entity :content-type "text/html")
    (normal-user request entity)))

(publish :path "/add-tendency" :function 'add-tendency)