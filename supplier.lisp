(in-package :groupbuy)

(define-url-function supplier (request "GroupBuy - Supplier" (message string "You are supplier."))
  (:p (fmt (format nil "Message: ~a" message)))
  (:p (:a :href "/show-tendency" "To show tendency")))