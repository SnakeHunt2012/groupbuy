(in-package :groupbuy)

(define-url-function show-tendency (request "GroupBuy - Show Tendency")
  (:p "All tendency in current list")
  (loop for tendency in *tendency-list* do
       (htm
	(:div :class "item"
	      (:a :href (format nil
				"/show-tendency-user?tendency-id=~a"
				(slot-value tendency 'id))
		  (:p
		   (fmt
		    (format nil
			    "tendency-id: ~a<br/>
                             destination: ~a<br/>
                             max distance: ~a<br/>
                             days in your hotel: ~a<br/>
                             time to arrive: ~a<br/>
                             room amount: ~a<br/>
                             room type: ~a<br/>
                             requirements: ~a<br/>
                             current vote: ~a<br/>"
			    (slot-value tendency 'id)
			    (slot-value tendency 'location)
			    (slot-value tendency 'distance)
			    (slot-value tendency 'days)
			    (slot-value tendency 'time)
			    (slot-value tendency 'room-amount)
			    (slot-value tendency 'room-type)
			    (slot-value tendency 'requirements)
			    (count-number (slot-value tendency 'user-list) 0)))))))))