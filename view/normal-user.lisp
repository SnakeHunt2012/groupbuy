(in-package :groupbuy)

(define-url-function random-number (request "GroupBuy - Normal User" (message string "This is just a test."))
     (:div (fmt (format nil "Message: ~a" message))))