(in-package :groupbuy)

;;; kernel

;; search-value-by-key:
;;
;; used in add-user-register
;; return value determined by the key you input
;; from cond's list such as
;; (("user-name" . "SnakeHunt2012")
;;  ("password" . "6666652")
;;  ("email" . "SnakeHunt2012@gmail.com")
;;  ("phone" . "18645659989"))
;; and you use this function like this
;; (search-value-by-key (the-list-below "email"))
(defun search-value-by-key (list string)
  (when list
    (if (string= (caar list) string)
	(cdar list)
	(search-value-by-key (cdr list) string))))

(defun count-number (list count)
  (if list
    (count-number (cdr list) (+ 1 count))
    count))

;;; tools

(defun add-user-register (request)
  (push
   (make-instance 'user
		  :type (if (search-value-by-key (request-query request) "type")
			    (parse-integer (search-value-by-key (request-query request) "type"))
			    2)
		  :user-name (search-value-by-key (request-query request) "user-name")
		  :password (search-value-by-key (request-query request) "password")
		  :profile (make-instance 'profile
					  :email (search-value-by-key (request-query request) "email")
					  :phone (search-value-by-key (request-query request) "phone")
					  :address (search-value-by-key (request-query request) "address")))
   *user-list*))

(defun login-check (request list type)
  (when list
      (if (and (string= (slot-value (car list) 'user-name)
			(search-value-by-key (request-query request) "user-name"))
	       (string= (slot-value (car list) 'password)
			(search-value-by-key (request-query request) "password"))
	       (= type (slot-value (car list) 'type)))
	  t
	  (login-check request (cdr list) type))))

(defun push-tendency (user-name location distance days time room-amount room-type requirements)
   (push
    (make-instance
     'tendency
     :user-list `(,user-name)
     :location location
     :distance distance
     :days days
     :time time
     :room-amount (parse-integer room-amount)
     :room-type room-type
     :requirements requirements)
    *tendency-list*))

;;; debug tools

;; show-user-list-debug
;;
;; list all user informaion in *user-list*
;; include those in profile of an user
;; usage (show-user-list-debug *user-list*)
(defun show-user-list-debug (list)
  (when list
    (format t "id: ~a~%" (slot-value (car list) 'id))
    (format t "user-name: ~a~%" (slot-value (car list) 'user-name))
    (format t "password: ~a~%" (slot-value (car list) 'password))
    (format t "type: ~a~%" (slot-value (car list) 'type))
    (format t "email: ~a~%" (slot-value (slot-value (car list) 'profile) 'email))
    (format t "phone: ~a~%" (slot-value (slot-value (car list) 'profile) 'phone))
    (format t "address: ~a~%" (slot-value (slot-value (car list) 'profile) 'address))
    (format t "~%")
    (show-user-list-debug (cdr list))))

(defun show-tendency-list-debug (list)
  (when list
    (format t "id: ~a~%" (slot-value (car list) 'id))
    (format t "user-list: ~a~%" (slot-value (car list) 'user-list))
    (format t "location: ~a~%" (slot-value (car list) 'location))
    (format t "distance: ~a~%" (slot-value (car list) 'distance))
    (format t "days: ~a~%" (slot-value (car list) 'days))
    (format t "time: ~a~%" (slot-value (car list) 'time))
    (format t "requirements: ~a~%" (slot-value (car list) 'requirements))
    (show-tendency-list-debug (cdr list))))