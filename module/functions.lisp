 ;;; load package
(in-package :groupbuy)

;;; login-check
(defun login-check (list user-name password)
    (if list
	(if (and (string= (slot-value (car list) 'user-name) user-name) (string= (slot-value (car list) 'password) password))
	    t
	    (login-check (cdr list) user-name password))
	nil))

;;; delete-user
(defun user-delete (list name)
  (if (null list)
      nil
      (if (string= (slot-value (car list) 'user-name) name)
	  (cdr list)
	  (append (list (car list)) (user-delete (cdr list) name)))))

(defun delete-user (name)
  (setf *user-list* (user-delete *user-list* name)))

;;; remove-tendency
(defun tendency-remove (list id)
  (if (null list)
      nil
      (if
       (= (slot-value (car list) 'tendency-id) id)
       (cdr list)
       (append (list (car list)) (tendency-remove (cdr list) id)))))

(defun remove-tendency (id)
  (setf *tendency-list* (tendency-remove *tendency-list* (parse-integer id))))

;;; search-by-tendency-id
(defun search-tendency-by-id (list id)
  (if (null list)
      (format t "wrong")
      (if (= (slot-value (car list) 'tendency-id) id)
	  (slot-value (car list) 'tendency-id)
	  (search-tendency-by-id (cdr list) id))))

(defun search-vote-by-id (list id)
  (if (null list)
      (format t "wrong")
      (if (= (slot-value (car list) 'tendency-id) id)
	  (slot-value (car list) 'vote)
	  (search-vote-by-id (cdr list) id))))

(defun set-tendency-by-id (list id new-value)
  (if (null list)
      (format t "wrong")
      (if (= (slot-value (car list) 'tendency-id) id)
	  (setf (slot-value (car list) 'tendency-id) new-value)
	  (set-tendency-by-id (cdr list) id new-value))))

(defun set-vote-by-id (list id new-vote)
  (if (null list)
      (format t "wrong")
      (if (= (slot-value (car list) 'tendency-id) id)
	  (setf (slot-value (car list) 'vote) new-vote)
	  (set-vote-by-id (cdr list) id new-vote))))

;;; offer-send
(defun offer-send (id)
  (> 1 (parse-integer id)))