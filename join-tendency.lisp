(in-package :groupbuy)

(defun join-tendency (request entity)
  (progn
    (loop for tendency in *tendency-list* do
	 (when (= (slot-value tendency 'id)
		  (parse-integer (search-value-by-key (request-query request) "tendency-id")))
	   (push
	    (search-value-by-key
	     (get-cookie-values request)
	     "user-name")
	    (slot-value tendency 'user-list))))
    (normal-user request entity)))

(publish :path "/join-tendency" :function 'join-tendency)