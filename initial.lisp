;;; load package
(in-package :groupbuy)

;;; initial root
(push
 (make-instance 'user :user-name "root" :password "6666652" :user-type 0)
 *user-list*)

(push
 (make-instance 'tendency
		:tendency-id 0
		:location "Da Zhi Street"
		:distance "100km"
		:days "one week"
		:price "20$"
		:requirements "Nothing else.") *tendency-list*)

(push
 (make-instance 'tendency
		:tendency-id 1
		:location "HIT"
		:distance "100m"
		:days "two months"
		:price "20$"
		:requirements "It can't be too near.") *tendency-list*)

(push
 (make-instance 'tendency
		:tendency-id 2
		:location "MIT"
		:distance "200m"
		:days "five years"
		:price "5$"
		:requirements "Nothing else") *tendency-list*)

(push
 (make-instance 'tendency
		:tendency-id 3
		:location "Da Zhi Street"
		:distance "500km"
		:days "three week"
		:price "20$"
		:requirements "Nothing else") *tendency-list*)

(setf *tendency-id* 3)
