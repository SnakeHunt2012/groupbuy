(in-package :groupbuy)

(defclass profile ()
  ((email
    :initarg :email
    :initform "")
   (phone
    :initarg :phone
    :initform "")
   (address
    :initarg :address
    :initform "")))

(defclass tendency ()
  ((id
    :initarg :id
    :initform (setf *tendency-id* (+ 1 *tendency-id*)))
   (user-list
    :initarg :user-list
    :initform '())
   (location
    :initarg :location
    :initform "")
   (distance
    :initarg :distance
    :initform "")
   (days
    :initarg :days
    :initform "")
   (time
    :initarg :time
    :initform "")
   (room-amount
    :initarg :room-amount
    :initform 1)
   (room-type
    :initarg :room-type
    :initform "small")
   (requirements
    :initarg :requirements
    :initform "")))

(defclass order ()
  ((id
    :initarg :id
    :initform (setf *order-id* (+ 1 *order-id*)))))

(defclass user ()
  ((id
    :initarg :id
    :initform (setf *user-id* (+ 1 *user-id*)))
   (user-name
    :initarg :user-name)
   (password
    :initarg :password)
   (type
    :initarg :type
    :initform 2)
   (profile

    :initarg :profile
    :initform (make-instance 'profile
			     :email ""
			     :phone ""
			     :address ""))
   (order-list
    :initarg :order-list
    :initform '())))